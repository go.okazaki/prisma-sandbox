import { PrismaClient } from '@prisma/client'

import {
  testCreateUser,
  testDocumentRequest,
  chance,
  testCreateTag,
} from '../util'
import {
  AllUsersDocument,
  AllUsersQueryVariables,
  CreateUserDocument,
  CreateUserMutationVariables,
  UpdateUserDocument,
  UpdateUserMutationVariables,
} from '../generated/client'

describe('prisma', () => {
  it('findMany', async () => {
    const prisma = new PrismaClient()
    const result = await prisma.user.findMany()
    expect(result).toBeInstanceOf(Array)
  })
})

describe('graphql', () => {
  it('mutation createUser', async () => {
    const user = await testCreateUser()
    expect(user).not.toBeUndefined()
  })

  it('mutation createUser validation', async () => {
    const response = await testDocumentRequest<CreateUserMutationVariables>(
      CreateUserDocument,
      {
        input: {
          email: 'xxxx',
          name: chance.name(),
        },
      },
    )
    expect(response.statusCode).toBe(400)
    expect(response.body?.errors).not.toBeUndefined()
    expect(response.body?.data?.createUser?.id).toBeUndefined()
  })

  it('mutation follow', async () => {
    const user1 = await testCreateUser()
    const user2 = await testCreateUser()

    const response = await testDocumentRequest<UpdateUserMutationVariables>(
      UpdateUserDocument,
      {
        input: {
          id: user1.id,
          addFollow: user2.id,
        },
      },
    )
    const updateUser = response.body.data.updateUser
    expect(updateUser.id).not.toBeUndefined()
    expect(updateUser.follows.totalCount).toBeGreaterThan(0)
  })

  it('mutation addTag', async () => {
    const user = await testCreateUser()
    const tag = await testCreateTag()

    const response = await testDocumentRequest<UpdateUserMutationVariables>(
      UpdateUserDocument,
      {
        input: {
          id: user.id,
          addTag: tag.id,
        },
      },
    )
    const updateUser = response.body.data.updateUser
    expect(updateUser.id).not.toBeUndefined()
    expect(updateUser.tags.totalCount).toBeGreaterThan(0)
  })

  it('query allUsers', async () => {
    const response = await testDocumentRequest<AllUsersQueryVariables>(
      AllUsersDocument,
      {
        connection: {},
      },
    )
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toMatch(/json/)
    expect(response.body).not.toBeUndefined()
  })
})
