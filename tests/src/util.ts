// https://github.com/visionmedia/supertest
import supertest, { Request } from 'supertest'
// https://github.com/prisma-labs/graphql-request
import { Variables } from 'graphql-request'
// https://github.com/chancejs/chancejs
import { Chance } from 'chance'

import app from '../../src/app'
import { DocumentNode, print } from 'graphql'
import {
  CreateTagDocument,
  CreateTagMutationVariables,
  CreateUserDocument,
  CreateUserMutationVariables,
  User,
} from './generated/client'

export const chance = new Chance()

export function testRequest(query: string, variables: any = {}): Request {
  const request = supertest(app)
    .post('/graphql')
    .set('Accept', 'application/json')
  return request.send({
    query,
    variables,
  })
}

export function testDocumentRequest<V = Variables>(
  document: DocumentNode,
  variables?: V,
): Request {
  return testRequest(print(document), variables)
}

export async function testCreateUser(): Promise<User> {
  const response = await testDocumentRequest<CreateUserMutationVariables>(
    CreateUserDocument,
    {
      input: {
        name: chance.name(),
        email: chance.email(),
      },
    },
  )
  return response.body.data.createUser
}

export async function testCreateTag(): Promise<User> {
  const response = await testDocumentRequest<CreateTagMutationVariables>(
    CreateTagDocument,
    {
      input: {
        name: chance.word(),
      },
    },
  )
  return response.body.data.createTag
}
