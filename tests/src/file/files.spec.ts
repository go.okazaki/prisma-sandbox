import supertest from 'supertest'

import app from '../../../src/app'
import { UploadDocument } from '../generated/client'
import { print } from 'graphql'

describe('file graphql', () => {
  it('mutation upload', async () => {
    // https://github.com/jaydenseric/graphql-multipart-request-spec
    const result = await supertest(app)
      .post('/graphql')
      .set('Accept', 'application/json')
      .field(
        'operations',
        JSON.stringify({
          query: print(UploadDocument),
          variables: {
            files: [null, null],
          },
        }),
      )
      .field(
        'map',
        JSON.stringify({
          0: ['variables.files.0'],
          1: ['variables.files.1'],
        }),
      )
      .attach('0', './tests/resources/3.png')
      .attach('1', './tests/resources/5.png')

    expect(result).not.toBeUndefined()
    expect(result.statusCode).toBe(200)

    const id1 = result.body.data.upload[0].id
    const downloadResult = await supertest(app).get(`/files/${id1}`)

    expect(downloadResult).not.toBeUndefined()
    expect(downloadResult.statusCode).toBe(200)
  })
})
