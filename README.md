# Prisma Sandbox

## Requirements

- GNU Make
- NodeJS
- Docker

## Build

```shell
make
```

## Development

```shell
docker-compose up
```

- [GraphQL Endpoint](http://localhost:3000/graphql)

### Prisma

- Generate Database Schema

```shell
npx prisma db push
```

- Generate Database Schema for Production

```shell
npx prisma migrate dev
npx prisma migrate deploy
```
