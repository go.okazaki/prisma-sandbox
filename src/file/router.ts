import { Request, Response, Router } from 'express'
import { getContext } from '../context'

const fileRouter = Router()

fileRouter.get('/files/:id', async (request: Request, response: Response) => {
  const id = request.params.id
  const context = getContext(request)
  const media = await context.prisma.file.findUnique({ where: { id } })
  if (media) {
    response.contentType('application/octet-stream')
    response.attachment(media.name)
    response.status(200).send(media.content)
  }
})

export { fileRouter }
