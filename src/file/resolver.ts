import { Prisma } from '@prisma/client'
import { toBuffer, toCursorConnection } from '../core'

import {
  File,
  FileConnection,
  MutationResolvers,
  QueryResolvers,
} from '../generated/graphql'

type FileEntity = {
  id: string
  name: string
  mimetype: string
  // content: Buffer
  contentLength: number
  createdAt: Date
  updatedAt: Date
}

const select = {
  id: true,
  name: true,
  mimetype: true,
  content: false,
  contentLength: true,
  createdAt: true,
  updatedAt: true,
}

function toFile(source: FileEntity): File {
  return {
    ...source,
  }
}

export const fileQueryResolvers: QueryResolvers = {
  files: async (_parent, args, context): Promise<File[]> => {
    const result = await context.prisma.file.findMany({
      select,
      where: {
        id: { in: args.ids },
      },
    })
    return result.map((r) => toFile(r))
  },
  allFiles: async (_parent, args, context): Promise<FileConnection> => {
    const baseArgs = {
      select,
      where: <Prisma.FileWhereInput>{
        createdAt: {
          gte: args.filter?.createdAt?.gte ?? undefined,
          lte: args.filter?.createdAt?.lte ?? undefined,
        },
      },
      orderBy: <Prisma.Enumerable<Prisma.FileOrderByWithRelationInput>>{
        createdAt: Prisma.SortOrder.desc,
      },
    }
    const result = await toCursorConnection<FileEntity, File>(
      (args) => context.prisma.file.findMany({ ...args, ...baseArgs }),
      () => context.prisma.file.count({ where: baseArgs.where }),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => toFile(record),
    )
    return result
  },
}

export const fileMutationResolvers: MutationResolvers = {
  upload: async (_parent, args, context): Promise<File[]> => {
    const results = await Promise.all(
      args.files.map(async (promise) => {
        const fileUpload = await promise
        const content = await toBuffer(fileUpload.createReadStream())
        const result = await context.prisma.file.create({
          data: {
            name: fileUpload.filename,
            mimetype: fileUpload.mimetype,
            content,
            contentLength: content.length,
          },
        })
        return result
      }),
    )
    return results.map((r) => toFile(r))
  },
}
