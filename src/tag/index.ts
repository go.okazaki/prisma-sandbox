import { Prisma, User as PUser, Tag as PTag } from '@prisma/client'
import { ClientError, emptyConnection, toCursorConnection } from '../core'
import {
  QueryResolvers,
  MutationResolvers,
  UserConnection,
  TagConnection,
  Tag,
  TagResolvers,
  User,
} from '../generated/graphql'
import { toUser } from '../user'

export function toTag(source: PTag): Tag {
  return {
    averageStars: 0,
    users: emptyConnection,
    voters: emptyConnection,
    ...source,
  }
}

export const tagQueryResolvers: QueryResolvers = {
  tags: async (_parent, args, context): Promise<Tag[]> => {
    const result = await context.prisma.tag.findMany({
      where: { id: { in: args.ids } },
    })
    return result.map((r) => toTag(r))
  },
  allTags: async (_parent, args, context): Promise<TagConnection> => {
    const baseArgs = {
      where: <Prisma.TagWhereInput>{
        // TODO
      },
      orderBy: <Prisma.Enumerable<Prisma.TagOrderByWithRelationInput>>{
        createdAt: Prisma.SortOrder.desc,
      },
    }
    const result = await toCursorConnection<PTag, Tag>(
      (args) => context.prisma.tag.findMany({ ...args, ...baseArgs }),
      () => context.prisma.tag.count({ where: baseArgs.where }),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => toTag(record),
    )
    return result
  },
}

export const tagMutationResolvers: MutationResolvers = {
  createTag: async (_parent, args, context): Promise<Tag> => {
    const result = await context.prisma.tag.create({
      data: {
        name: args.input.name,
      },
    })
    return toTag(result)
  },
  // updateTag: async (_parent, args, context): Promise<Tag> => {
  //   const id = args.input.id
  //   const userId = context.getUser().id
  //   const result = await context.prisma.tag.update({
  //     where: { id },
  //     data: {
  //       name: args.input.name ?? undefined,
  //       votedUsers: {
  //         connectOrCreate: args.input.vote
  //           ? {
  //               where: {
  //                 userId_tagId: {
  //                   tagId: id,
  //                   userId,
  //                 },
  //               },
  //               create: {
  //                 userId,
  //               },
  //             }
  //           : undefined,
  //       },
  //     },
  //   })
  //   if (!result) {
  //     throw new ClientError(`Tag id:${args.input.id} not found`)
  //   }
  //   return toTag(result)
  // },
}

export const tagResolvers: TagResolvers = {
  averageStars: async (parent, args, context): Promise<number> => {
    // TODO
    return 0
  },
  users: async (parent, args, context): Promise<UserConnection> => {
    const baseArgs = {
      where: <Prisma.UserWhereInput>{
        tags: {
          some: {
            tagId: parent.id,
          },
        },
      },
    }
    const result = await toCursorConnection<PUser, User>(
      (args) => context.prisma.user.findMany({ ...args, ...baseArgs }),
      () => context.prisma.user.count(baseArgs),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => toUser(record),
    )
    return result
  },
  voters: async (parent, args, context): Promise<UserConnection> => {
    const baseArgs = {
      where: <Prisma.UserWhereInput>{
        voteTags: {
          some: {
            tagId: parent.id,
          },
        },
      },
    }
    const result = await toCursorConnection<PUser, User>(
      (args) => context.prisma.user.findMany({ ...args, ...baseArgs }),
      () => context.prisma.user.count(baseArgs),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => toUser(record),
    )
    return result
  },
}
