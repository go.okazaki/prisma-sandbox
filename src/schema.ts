import path from 'path'
import { loadSchemaSync } from '@graphql-tools/load'
import { makeExecutableSchema } from '@graphql-tools/schema'
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader'
// https://github.com/jaydenseric/graphql-upload#class-graphqlupload
import { GraphQLUpload } from 'graphql-upload'
import { constraintDirective } from 'graphql-constraint-directive'

import { Resolvers } from './generated/graphql'
import { getConfig } from './core'
import {
  userMutationResolvers,
  userQueryResolvers,
  userResolvers,
} from './user'
import { fileMutationResolvers, fileQueryResolvers } from './file'
import { tagMutationResolvers, tagQueryResolvers, tagResolvers } from './tag'

const resolvers: Resolvers = {
  Query: {
    ...userQueryResolvers,
    ...fileQueryResolvers,
    ...tagQueryResolvers,
  },
  Mutation: {
    ...userMutationResolvers,
    ...fileMutationResolvers,
    ...tagMutationResolvers,
  },
  User: {
    ...userResolvers,
  },
  Tag: {
    ...tagResolvers,
  },
  Upload: GraphQLUpload,
}

const schemaPath = path.join(getConfig().resourceBaseDir, 'schema.graphql')
const schema = makeExecutableSchema({
  typeDefs: [
    loadSchemaSync(schemaPath, {
      loaders: [new GraphQLFileLoader()],
    }),
  ],
  resolvers,
})

export default constraintDirective()(schema)
