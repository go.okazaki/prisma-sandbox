// https://github.com/moment/luxon/
import { DateTime } from 'luxon'

export function isValidDate(value: string | Date | undefined): boolean {
  return value instanceof Date && !Number.isNaN(value.getTime())
}

export function parseDate(
  value: string | Date | undefined,
  format: string,
  zone?: string,
): Date | undefined {
  if (value == undefined || value instanceof Date) {
    return value
  }
  const dateTime = DateTime.fromFormat(value, format, { zone })
  let date: Date | undefined = dateTime.toJSDate()
  if (!isValidDate(date)) {
    console.warn('util', `Invalid Date value:${JSON.stringify(value)}`)
    date = undefined
  }
  return date
}

export function formatDate(date: Date, format: string, zone?: string): string {
  const dateTime = DateTime.fromJSDate(date, { zone })
  const result = dateTime.toFormat(format)
  if (result === 'Invalid DateTime') {
    throw new Error(`Invalid DateTime date:${date} zone:${zone}`)
  }
  return result
}
