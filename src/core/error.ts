import { ExecutionResult, GraphQLError } from 'graphql'

export class ServiceError<T> extends Error {
  readonly reason: T | undefined

  constructor(message: string, reason?: T) {
    super(message)
    this.name = this.constructor.name
    this.reason = reason
  }
}

export class ClientError<T> extends ServiceError<T> {
  constructor(message: string, reason?: T) {
    super(message, reason)
  }
}

export class ServerError<T> extends ServiceError<T> {
  constructor(message: string, reason?: T) {
    super(message, reason)
  }
}

export function handleGraphQLErrors(result: ExecutionResult): ExecutionResult {
  let clientError: Error | undefined
  result.errors?.forEach((error, _index) => {
    console.error(error)
    if (!clientError) {
      if (error instanceof ClientError) {
        clientError = error
      } else if (error instanceof GraphQLError) {
        clientError = error
      }
      clientError = error
    }
  })
  if (clientError) throw clientError
  return result
}
