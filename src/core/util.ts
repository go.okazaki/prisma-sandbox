import assert from 'assert'
import { Readable } from 'stream'

export function notEmpty<T>(value: T | null | undefined): value is T {
  return value !== null && value !== undefined
}

export function assertNotEmpty<T>(value: T | null | undefined): T {
  assert(value)
  return value
}

export function toBuffer(readable: Readable): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    const chunks: Buffer[] = []
    readable.on('data', (chunk: Buffer) => {
      chunks.push(chunk)
    })
    readable.on('close', () => {
      resolve(Buffer.concat(chunks))
    })
    readable.on('error', reject)
  })
}

export function encodeBase64<T>(value: T): string {
  return Buffer.from(JSON.stringify(value)).toString('base64')
}

export function decodeBase64<T>(value: string): T {
  return JSON.parse(Buffer.from(value, 'base64').toString('ascii'))
}
