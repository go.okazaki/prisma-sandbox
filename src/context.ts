import { Request } from 'express'
import { Prisma, PrismaClient } from '@prisma/client'

export interface Context {
  prisma: PrismaClient
}

const prismaLogLevels: Array<Prisma.LogLevel> = [
  'query',
  // 'info',
  'warn',
  'error',
]
const prisma = new PrismaClient({
  log: prismaLogLevels,
})

export const context: Context = {
  prisma: prisma,
}

export function getContext(req: Request): Context {
  return {
    prisma,
  }
}
