import app from './app'

const port = Number(process.env.PORT)
const server = app.listen(port, () => {
  console.log(
    `🚀 Server ${process.env.npm_package_name} version:${process.env.npm_package_version} ready at http://localhost:${port}`,
  )
})

function shutdown() {
  server.close(() => {
    console.log('stop server')
    process.exit(0)
  })
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)

export default server
