import { Prisma, User as PUser, UserTags } from '@prisma/client'
import { ClientError, emptyConnection, toCursorConnection } from '../core'
import {
  QueryResolvers,
  MutationResolvers,
  User,
  UserResolvers,
  UserConnection,
  UserTagConnection,
} from '../generated/graphql'
import { toTag } from '../tag'

export function toUser(source: PUser): User {
  return {
    follows: emptyConnection,
    followers: emptyConnection,
    tags: emptyConnection,
    ...source,
  }
}

export const userQueryResolvers: QueryResolvers = {
  users: async (_parent, args, context): Promise<User[]> => {
    const result = await context.prisma.user.findMany({
      where: { id: { in: args.ids } },
    })
    return result.map((r) => toUser(r))
  },
  allUsers: async (_parent, args, context): Promise<UserConnection> => {
    const baseArgs = {
      where: <Prisma.UserWhereInput>{
        // TODO
      },
      orderBy: <Prisma.Enumerable<Prisma.UserOrderByWithRelationInput>>{
        createdAt: Prisma.SortOrder.desc,
      },
    }
    const result = await toCursorConnection<PUser, User>(
      (args) => context.prisma.user.findMany({ ...args, ...baseArgs }),
      () => context.prisma.user.count({ where: baseArgs.where }),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => toUser(record),
    )
    return result
  },
  findUser: async (_parent, args, context): Promise<User | null> => {
    const result = await context.prisma.user.findUnique({
      where: { email: args.email },
    })
    return result ? toUser(result) : null
  },
}

export const userMutationResolvers: MutationResolvers = {
  createUser: async (_parent, args, context): Promise<User> => {
    const result = await context.prisma.user.create({
      data: {
        name: args.input.name,
        email: args.input.email,
        enabled: false,
      },
    })
    return toUser(result)
  },
  updateUser: async (_parent, args, context): Promise<User> => {
    const id = args.input.id
    const result = await context.prisma.user.update({
      where: { id },
      data: {
        name: args.input.name,
        enabled: args.input.enabled ?? undefined,
        followers: {
          connectOrCreate: args.input.addFollow
            ? {
                where: {
                  followerId_followId: {
                    followId: args.input.addFollow,
                    followerId: id,
                  },
                },
                create: {
                  followId: args.input.addFollow,
                },
              }
            : undefined,
          delete: args.input.removeFollow
            ? {
                followerId_followId: {
                  followId: args.input.removeFollow,
                  followerId: id,
                },
              }
            : undefined,
        },
        tags: {
          connectOrCreate: args.input.addTag
            ? {
                where: {
                  userId_tagId: {
                    tagId: args.input.addTag,
                    userId: id,
                  },
                },
                create: {
                  tagId: args.input.addTag,
                },
              }
            : undefined,
          delete: args.input.removeTag
            ? {
                userId_tagId: {
                  tagId: args.input.removeTag,
                  userId: id,
                },
              }
            : undefined,
        },
      },
    })
    if (!result) {
      throw new ClientError(`User id:${args.input.id} not found`)
    }
    return toUser(result)
  },
}

export const userResolvers: UserResolvers = {
  follows: async (parent, args, context): Promise<UserConnection> => {
    const baseArgs = {
      where: {
        follows: {
          some: {
            followerId: parent.id,
          },
        },
      },
    }
    const result = await toCursorConnection<PUser, User>(
      (args) => context.prisma.user.findMany({ ...args, ...baseArgs }),
      () => context.prisma.user.count(baseArgs),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => toUser(record),
    )
    return result
  },
  followers: async (parent, args, context): Promise<UserConnection> => {
    const baseArgs = {
      where: <Prisma.UserWhereInput>{
        followers: {
          some: {
            followId: parent.id,
          },
        },
      },
    }
    const result = await toCursorConnection<PUser, User>(
      (args) => context.prisma.user.findMany({ ...args, ...baseArgs }),
      () => context.prisma.user.count(baseArgs),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => toUser(record),
    )
    return result
  },
  tags: async (parent, args, context): Promise<UserTagConnection> => {
    const baseArgs = {
      where: <Prisma.UserTagsWhereInput>{
        userId: parent.id,
      },
      orderBy: <Prisma.Enumerable<Prisma.UserTagsOrderByWithRelationInput>>{
        createdAt: Prisma.SortOrder.desc,
      },
    }
    const tagIds: string[] = []
    const result = await toCursorConnection<
      UserTags,
      UserTags,
      Prisma.UserTagsWhereUniqueInput
    >(
      (args) => context.prisma.userTags.findMany({ ...args, ...baseArgs }),
      () => context.prisma.userTags.count(baseArgs),
      { first: args.connection?.first, after: args.connection?.after },
      (record) => {
        tagIds.push(record.tagId)
        return record
      },
      (record) => ({
        userId_tagId: { userId: record.userId, tagId: record.tagId },
      }),
    )
    const tags = await context.prisma.tag.findMany({
      where: { id: { in: tagIds } },
    })
    return {
      ...result,
      edges: result.edges.map((e) => {
        const node = e.node
        const tag = tags.find((t) => t.id === node.tagId)
        if (!tag) {
          throw new ClientError(`Tag id:${node.tagId} not found`)
        }
        return {
          ...e,
          node: {
            ...node,
            userId: node.userId,
            tag: toTag(tag),
            ratings: [],
          },
        }
      }),
    }
  },
}
